import { ThirdwebNftMedia, useAddress, useNFT } from "@thirdweb-dev/react";
import { EditionDrop, NFT, SmartContract } from "@thirdweb-dev/sdk";
import React, { useEffect, useState } from "react";
import ContractMappingResponse from "../types/ContractMappingResponse";
import { INITIAL_TOKEN_SYMBOL } from "../const/contract";
import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Image from 'next/image';
import Play from "./Animation";
import { swapUrl } from "../const/aLinks";
import styles from "../styles/Home.module.scss";
import MiningAnim from "../styles/Anim.module.scss";

const Symbol = INITIAL_TOKEN_SYMBOL;

type Props = {
  miningContract: SmartContract<any>;
  characterContract: EditionDrop;
  initialContract: EditionDrop;
};

const swapToken = swapUrl;

export default function CurrentGear({
  miningContract,
  characterContract,
  initialContract,
}: Props) {
  const address = useAddress();

  const { data: playerNft } = useNFT(characterContract, 1);
  const [initial, setInitial] = useState<NFT>();


const Cloud = (
  <div className={MiningAnim.slideCloud}>
    <Image className={MiningAnim.awan} style={{zIndex: '-1px'}} src="/cloud.png" height={260} width={525} alt="cloud" />
  </div>
);

  useEffect(() => {
    (async () => {
      if (!address) return;

      const p = (await miningContract.call(
        "playerNFT",
        address
      )) as ContractMappingResponse;


      if (p.isData) {
        const initialMetadata = await initialContract.get(p.value);
        setInitial(initialMetadata);
      }
    })();
  }, [address, miningContract, initialContract]);

  return (
<>
{initial ? (
<Card style={{backgroundColor: 'transparent'}}>
    <div className={MiningAnim.sliderBox}>
      <div className={MiningAnim.sliderCloud}>
{Cloud}
{Cloud}
{Cloud}
{Cloud}
{Cloud}
      </div>
      <div
        style={{
          display: "flex",
          padding: '0 10px',
          alignItems: "center",
          flexDirection: "row",
          justifyContent: "space-between",
          marginBottom: '40px',
          zIndex: '1'
        }}
      >
        {/* Currently equipped player */}
        <div style={{ borderRadius: 16, height: 40, overflow: 'hidden' }}>
          {playerNft && (
            <ThirdwebNftMedia metadata={playerNft?.metadata} height={"40"} />
          )}
        </div>
        {/* Currently equipped initial */}
        <div
          style={{ borderRadius: 16, marginLeft: 8, height: 40, overflow: 'hidden' }}
        >
          {initial && (
            // @ts-ignore
            <ThirdwebNftMedia metadata={initial.metadata} height={"40"} />
          )}
        </div>
      </div>
{initial && (
            // @ts-ignore
      <span className={`${styles.noGapTop} `}></span>
    )}

      {/* MiningAnim Animation */}

      <div
        style={{
          display: "flex",
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "flex-start",
          marginTop: 33,
        }}
      >
        <Image className={MiningAnim.motor1} src="/motor.png" height={64} width={64} alt="character-mining" />
        <Play initial={initial} />
      </div>
    </div>
</Card>
) : ( 
<>
<Card style={{ minHeight: '224.05px' }}>
    <Card.Body className={MiningAnim.sliderBox} style={{padding: '10px', textAlign: 'left', color: '#121212'}}>
<Card.Title>Cara Mining Token{Symbol}</Card.Title>
<span> Untuk memulai, kamu harus memiliki kartu level yang akan otomatis mengaktifkan fungsi mining.<br/>Setiap <b>kartu</b> mempunyai tingkat kecepatan mining yang berbeda, kamu bisa memulai dengan kartu <b>"Level #0"</b> yang tersedia di shop <b>*GRATIS</b> untuk mulai mengumpulkan token <b>{Symbol}</b> yang maksimal, Token <b>{Symbol}</b> bisa ditukarkan ke token <b>MATIC</b> (polygon) di <a href={swapToken()} target='_blank' rel="noreferrer"><b>UniSwap</b></a>.</span>
    </Card.Body>
</Card>
</>
    )}
</>
  );
}
